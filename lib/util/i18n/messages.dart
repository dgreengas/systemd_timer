import 'package:intl/intl.dart';

class Messages {
  static String applicationTitle() => Intl.message("Scheduled Tasks",
      name: "applicationTitle", desc: "The title of the application");

  static String newTaskTitle() => Intl.message("Schedule a new task",
      name: 'newTaskTitle',
      desc: "Title for the page when scheduling a new task");
  static String dayOfWeek() => Intl.message("Day of the Week",
      name: 'dayOfWeek',
      desc: "Pick the day of week when this task should run");
  static String hourOfDay() =>
      Intl.message("During what hours should the taks run?",
          name: 'hourOfDay', desc: "For which hours should the task run");

  static String minuteOfDay() => Intl.message(
      "On what what minutes should the task run (Example: 0,15,30,45 is every 15 minutes)?",
      name: 'minuteOfDay',
      desc: "For which minutes should the task run");

  static String secondOfDay() => Intl.message(
      "On what what seconds should the task run (Example: 0,30 is every 30 seconds)??",
      name: 'secondOfDay',
      desc: "For which seconds should the task run");

  static String everyday() => Intl.message("Everyday",
      name: "everyday", desc: "The task should run everyday");

  static String everyHour() => Intl.message("Every Hour",
      name: "everyhour", desc: "The task should run every hour");

  static String everyMinute() => Intl.message("Every Minute ",
      name: "everyminute", desc: "The task should run every minute");

  static String everySecond() => Intl.message("Every Second",
      name: "everysecond", desc: "The task should run every second");
  static String monday() =>
      Intl.message("Monday", name: 'monday', desc: "Day of week - Monday");
  static String mondayAbbr() => Intl.message("Mon",
      name: 'mondayabbr', desc: "Short abbreviation for day of week - Monday");

  static String tuesday() =>
      Intl.message("Tuesday", name: 'tuesday', desc: "Day of week - Tuesday");
  static String tuesdayAbbr() => Intl.message("Tue",
      name: 'tuesdayabbr',
      desc: "Short abbreviation for day of week - Tuesday");

  static String wednesday() => Intl.message("Wednesday",
      name: 'wednesday', desc: "Day of week - Wednesday");
  static String wednesdayAbbr() => Intl.message("Wed",
      name: 'wednesdayabbr',
      desc: "Short abbreviation for day of week - Wednesday");
  static String thursday() => Intl.message("Thursday",
      name: 'thursday', desc: "Day of week - Thursday");
  static String thursdayAbbr() => Intl.message("Thu",
      name: 'thursdayabbr',
      desc: "Short abbreviation for day of week - Thursday");
  static String friday() =>
      Intl.message("Friday", name: 'friday', desc: "Day of week - Friday");
  static String fridayAbbr() => Intl.message("Fri",
      name: 'fridayabbr', desc: "Short abbreviation for day of week - Friday");
  static String saturday() => Intl.message("Saturday",
      name: 'saturday', desc: "Day of week - Saturday");
  static String saturdayAbbr() => Intl.message("Sat",
      name: 'saturdayabbr',
      desc: "Short abbreviation for day of week - Saturday");

  static String sunday() =>
      Intl.message("Sunday", name: 'sunday', desc: "Day of week - Sunday");
  static String sundayAbbr() => Intl.message("Sun",
      name: 'sundayabbr', desc: "Short abbreviation for day of week - Sunday");

  static String hour() =>
      Intl.message("Hour", name: "hour", desc: "Hour as represented in a time");

  static String minute() => Intl.message("Minute",
      name: "minute", desc: "Minute as represented in a time");

  static String description() => Intl.message("Description",
      name: "description", desc: "The description of the timer");
}
