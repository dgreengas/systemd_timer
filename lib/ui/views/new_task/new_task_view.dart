import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:intl/intl.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_hooks/stacked_hooks.dart';
import 'package:systemd_timer/ui/views/new_task/new_task_viewmodel.dart';
import 'package:systemd_timer/util/i18n/messages.dart';

class NewTaskView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final MaterialLocalizations localizations =
        Localizations.of<MaterialLocalizations>(context, MaterialLocalizations);

    return ViewModelBuilder<NewTaskViewModel>.reactive(
        builder: (BuildContext context, NewTaskViewModel model, Widget child) =>
            Scaffold(
              appBar: AppBar(
                title: Text(Messages.newTaskTitle()),
                centerTitle: true,
                actions: [
                  IconButton(
                    icon: Icon(Icons.save),
                    onPressed: () => model.saveTask(),
                    tooltip: localizations.saveButtonLabel,
                  )
                ],
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(model.nextElapsed),
                      _DescriptionForm(),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        Messages.dayOfWeek(),
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ChoiceChip(
                            label: Text(Messages.everyday()),
                            selected: model.everyDay,
                            onSelected: (value) => model.runEveryDay(value)),
                      ),
                      Wrap(
                          children: List.generate(
                        7,
                        (index) => ChoiceChip(
                          label: Text(localizations.narrowWeekdays[
                              (index + localizations.firstDayOfWeekIndex) % 7]),
                          onSelected: (value) => model.selectDay(
                              (index + localizations.firstDayOfWeekIndex) % 7,
                              value),
                          selected: model.daySelected[
                              (index + localizations.firstDayOfWeekIndex) % 7],
                        ),
                      )),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        Messages.hourOfDay(),
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ChoiceChip(
                            label: Text(Messages.everyHour()),
                            selected: model.everyHour,
                            onSelected: (value) => model.runEveryHour(value)),
                      ),
                      Wrap(
                        children: List.generate(
                            24,
                            (index) => ChoiceChip(
                                label: Text(index.toString()),
                                onSelected: (value) =>
                                    model.selectHour(index, value),
                                selected: model.hourSelected[index])),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        Messages.minuteOfDay(),
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ChoiceChip(
                            label: Text(Messages.everyMinute()),
                            selected: model.everyMinute,
                            onSelected: (value) => model.runEveryMinute(value)),
                      ),
                      Wrap(
                        children: List.generate(
                            60,
                            (index) => ChoiceChip(
                                label: Text(index.toString()),
                                onSelected: (value) =>
                                    model.selectMinute(index, value),
                                selected: model.minuteSelected[index])),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        Messages.secondOfDay(),
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ChoiceChip(
                            label: Text(Messages.everySecond()),
                            selected: model.everySecond,
                            onSelected: (value) => model.runEverySecond(value)),
                      ),
                      Wrap(
                        children: List.generate(
                            60,
                            (index) => ChoiceChip(
                                label: Text(index.toString()),
                                onSelected: (value) =>
                                    model.selectSecond(index, value),
                                selected: model.secondSelected[index])),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            flex: 3,
                            child: Text(
                              model.scriptPath,
                              style: TextStyle(fontStyle: FontStyle.italic),
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.loose,
                            flex: 1,
                            child: IconButton(
                                icon: Icon(Icons.code),
                                onPressed: () => model.chooseScript()),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
        viewModelBuilder: () => NewTaskViewModel());
  }
}

class _DescriptionForm extends HookViewModelWidget<NewTaskViewModel> {
  _DescriptionForm({Key key}) : super(key: key, reactive: false);

  @override
  Widget buildViewModelWidget(
      BuildContext context, NewTaskViewModel viewModel) {
    var text = useTextEditingController();

    return TextField(
      controller: text,
      onChanged: viewModel.setDescription,
      decoration: InputDecoration(
        icon: Icon(Icons.note),
        hintText: Messages.description(),
      ),
    );
  }
}
