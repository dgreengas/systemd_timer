import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:systemd_timer/app/locator.dart';
import 'package:systemd_timer/app/router.gr.dart';
import 'package:systemd_timer/util/i18n/messages.dart';

class HomeViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();

  String title = Messages.applicationTitle();

  Future navigateToNewTask() async {
    await _navigationService.navigateTo(Routes.newTaskView);
  }
}
