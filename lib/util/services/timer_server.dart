// This file was generated using the following command and may be overwritten.
// dart-dbus generate-remote-object lib/util/services/timer_server.xml

import 'package:dbus/dbus.dart';

/// Signal data for org.freedesktop.DBus.Properties.PropertiesChanged.
class OrgFreedesktopDBusPeerPropertiesChanged extends DBusSignal {
  String get interface => (values[0] as DBusString).value;
  Map<String, DBusValue> get changedProperties =>
      (values[1] as DBusDict).children.map((key, value) =>
          MapEntry((key as DBusString).value, (value as DBusVariant).value));
  List<String> get invalidatedProperties => (values[2] as DBusArray)
      .children
      .map((child) => (child as DBusString).value)
      .toList();

  OrgFreedesktopDBusPeerPropertiesChanged(DBusSignal signal)
      : super(signal.sender, signal.path, signal.interface, signal.member,
            signal.values);
}

class OrgFreedesktopDBusPeer extends DBusRemoteObject {
  OrgFreedesktopDBusPeer(DBusClient client, String destination,
      {DBusObjectPath path = const DBusObjectPath.unchecked('null')})
      : super(client, destination, path);

  /// Invokes org.freedesktop.DBus.Peer.Ping()
  Future callPing() async {
    await callMethod('org.freedesktop.DBus.Peer', 'Ping', []);
  }

  /// Invokes org.freedesktop.DBus.Peer.GetMachineId()
  Future<String> callGetMachineId() async {
    var result =
        await callMethod('org.freedesktop.DBus.Peer', 'GetMachineId', []);
    return (result.returnValues[0] as DBusString).value;
  }

  /// Invokes org.freedesktop.DBus.Introspectable.Introspect()
  Future<String> callIntrospect() async {
    var result = await callMethod(
        'org.freedesktop.DBus.Introspectable', 'Introspect', []);
    return (result.returnValues[0] as DBusString).value;
  }

  /// Invokes org.freedesktop.DBus.Properties.Get()
  Future<DBusValue> callGet(String interface, String property) async {
    var result = await callMethod('org.freedesktop.DBus.Properties', 'Get',
        [DBusString(interface), DBusString(property)]);
    return (result.returnValues[0] as DBusVariant).value;
  }

  /// Invokes org.freedesktop.DBus.Properties.GetAll()
  Future<Map<String, DBusValue>> callGetAll(String interface) async {
    var result = await callMethod(
        'org.freedesktop.DBus.Properties', 'GetAll', [DBusString(interface)]);
    return (result.returnValues[0] as DBusDict).children.map((key, value) =>
        MapEntry((key as DBusString).value, (value as DBusVariant).value));
  }

  /// Invokes org.freedesktop.DBus.Properties.Set()
  Future callSet(String interface, String property, DBusValue value) async {
    await callMethod('org.freedesktop.DBus.Properties', 'Set',
        [DBusString(interface), DBusString(property), DBusVariant(value)]);
  }

  /// Subscribes to org.freedesktop.DBus.Properties.PropertiesChanged.
  Stream<DBusPropertiesChangedSignal> subscribePropertiesChanged() async* {
    var signals =
        subscribeSignal('org.freedesktop.DBus.Properties', 'PropertiesChanged');
    await for (var signal in signals) {
      if (signal.values.length == 3 &&
          signal.values[0].signature == DBusSignature('s') &&
          signal.values[1].signature == DBusSignature('a{sv}') &&
          signal.values[2].signature == DBusSignature('as')) {
        yield DBusPropertiesChangedSignal(signal);
      }
    }
  }

  /// Gets org.freedesktop.systemd1.Timer.Unit
  Future<String> getUnit() async {
    var value = await getProperty('org.freedesktop.systemd1.Timer', 'Unit');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.TimersMonotonic
  Future<List<DBusValue>> getTimersMonotonic() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'TimersMonotonic');
    return (value as DBusArray).children.map((child) => child).toList();
  }

  /// Gets org.freedesktop.systemd1.Timer.TimersCalendar
  Future<List<DBusValue>> getTimersCalendar() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'TimersCalendar');
    return (value as DBusArray).children.map((child) => child).toList();
  }

  /// Gets org.freedesktop.systemd1.Timer.OnClockChange
  Future<bool> getOnClockChange() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'OnClockChange');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.OnTimezoneChange
  Future<bool> getOnTimezoneChange() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'OnTimezoneChange');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.NextElapseUSecRealtime
  Future<int> getNextElapseUSecRealtime() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Timer', 'NextElapseUSecRealtime');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.NextElapseUSecMonotonic
  Future<int> getNextElapseUSecMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Timer', 'NextElapseUSecMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.LastTriggerUSec
  Future<int> getLastTriggerUSec() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'LastTriggerUSec');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.LastTriggerUSecMonotonic
  Future<int> getLastTriggerUSecMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Timer', 'LastTriggerUSecMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.Result
  Future<String> getResult() async {
    var value = await getProperty('org.freedesktop.systemd1.Timer', 'Result');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.AccuracyUSec
  Future<int> getAccuracyUSec() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'AccuracyUSec');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.RandomizedDelayUSec
  Future<int> getRandomizedDelayUSec() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Timer', 'RandomizedDelayUSec');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.Persistent
  Future<bool> getPersistent() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'Persistent');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.WakeSystem
  Future<bool> getWakeSystem() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Timer', 'WakeSystem');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Timer.RemainAfterElapse
  Future<bool> getRemainAfterElapse() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Timer', 'RemainAfterElapse');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Id
  Future<String> getId() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Id');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Names
  Future<List<String>> getNames() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Names');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Following
  Future<String> getFollowing() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Following');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Requires
  Future<List<String>> getRequires() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Requires');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Requisite
  Future<List<String>> getRequisite() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Requisite');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Wants
  Future<List<String>> getWants() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Wants');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.BindsTo
  Future<List<String>> getBindsTo() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'BindsTo');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.PartOf
  Future<List<String>> getPartOf() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'PartOf');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.RequiredBy
  Future<List<String>> getRequiredBy() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'RequiredBy');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.RequisiteOf
  Future<List<String>> getRequisiteOf() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'RequisiteOf');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.WantedBy
  Future<List<String>> getWantedBy() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'WantedBy');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.BoundBy
  Future<List<String>> getBoundBy() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'BoundBy');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.ConsistsOf
  Future<List<String>> getConsistsOf() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'ConsistsOf');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Conflicts
  Future<List<String>> getConflicts() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Conflicts');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.ConflictedBy
  Future<List<String>> getConflictedBy() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'ConflictedBy');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Before
  Future<List<String>> getBefore() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Before');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.After
  Future<List<String>> getAfter() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'After');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.OnFailure
  Future<List<String>> getOnFailure() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'OnFailure');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Triggers
  Future<List<String>> getTriggers() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Triggers');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.TriggeredBy
  Future<List<String>> getTriggeredBy() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'TriggeredBy');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.PropagatesReloadTo
  Future<List<String>> getPropagatesReloadTo() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'PropagatesReloadTo');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.ReloadPropagatedFrom
  Future<List<String>> getReloadPropagatedFrom() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ReloadPropagatedFrom');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.JoinsNamespaceOf
  Future<List<String>> getJoinsNamespaceOf() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'JoinsNamespaceOf');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.RequiresMountsFor
  Future<List<String>> getRequiresMountsFor() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'RequiresMountsFor');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Documentation
  Future<List<String>> getDocumentation() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'Documentation');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Description
  Future<String> getDescription() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'Description');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.LoadState
  Future<String> getLoadState() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'LoadState');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ActiveState
  Future<String> getActiveState() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'ActiveState');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.SubState
  Future<String> getSubState() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'SubState');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.FragmentPath
  Future<String> getFragmentPath() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'FragmentPath');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.SourcePath
  Future<String> getSourcePath() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'SourcePath');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.DropInPaths
  Future<List<String>> getDropInPaths() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'DropInPaths');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.UnitFileState
  Future<String> getUnitFileState() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'UnitFileState');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.UnitFilePreset
  Future<String> getUnitFilePreset() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'UnitFilePreset');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.StateChangeTimestamp
  Future<int> getStateChangeTimestamp() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'StateChangeTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.StateChangeTimestampMonotonic
  Future<int> getStateChangeTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'StateChangeTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.InactiveExitTimestamp
  Future<int> getInactiveExitTimestamp() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'InactiveExitTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.InactiveExitTimestampMonotonic
  Future<int> getInactiveExitTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'InactiveExitTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ActiveEnterTimestamp
  Future<int> getActiveEnterTimestamp() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ActiveEnterTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ActiveEnterTimestampMonotonic
  Future<int> getActiveEnterTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ActiveEnterTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ActiveExitTimestamp
  Future<int> getActiveExitTimestamp() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ActiveExitTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ActiveExitTimestampMonotonic
  Future<int> getActiveExitTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ActiveExitTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.InactiveEnterTimestamp
  Future<int> getInactiveEnterTimestamp() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'InactiveEnterTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.InactiveEnterTimestampMonotonic
  Future<int> getInactiveEnterTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'InactiveEnterTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.CanStart
  Future<bool> getCanStart() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'CanStart');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.CanStop
  Future<bool> getCanStop() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'CanStop');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.CanReload
  Future<bool> getCanReload() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'CanReload');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.CanIsolate
  Future<bool> getCanIsolate() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'CanIsolate');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.CanClean
  Future<List<String>> getCanClean() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'CanClean');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Job
  Future<DBusValue> getJob() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Job');
    return value;
  }

  /// Gets org.freedesktop.systemd1.Unit.StopWhenUnneeded
  Future<bool> getStopWhenUnneeded() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'StopWhenUnneeded');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.RefuseManualStart
  Future<bool> getRefuseManualStart() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'RefuseManualStart');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.RefuseManualStop
  Future<bool> getRefuseManualStop() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'RefuseManualStop');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.AllowIsolate
  Future<bool> getAllowIsolate() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'AllowIsolate');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.DefaultDependencies
  Future<bool> getDefaultDependencies() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'DefaultDependencies');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.OnFailureJobMode
  Future<String> getOnFailureJobMode() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'OnFailureJobMode');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.IgnoreOnIsolate
  Future<bool> getIgnoreOnIsolate() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'IgnoreOnIsolate');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.NeedDaemonReload
  Future<bool> getNeedDaemonReload() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'NeedDaemonReload');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.JobTimeoutUSec
  Future<int> getJobTimeoutUSec() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'JobTimeoutUSec');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.JobRunningTimeoutUSec
  Future<int> getJobRunningTimeoutUSec() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'JobRunningTimeoutUSec');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.JobTimeoutAction
  Future<String> getJobTimeoutAction() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'JobTimeoutAction');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.JobTimeoutRebootArgument
  Future<String> getJobTimeoutRebootArgument() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'JobTimeoutRebootArgument');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ConditionResult
  Future<bool> getConditionResult() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'ConditionResult');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.AssertResult
  Future<bool> getAssertResult() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'AssertResult');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ConditionTimestamp
  Future<int> getConditionTimestamp() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ConditionTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.ConditionTimestampMonotonic
  Future<int> getConditionTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'ConditionTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.AssertTimestamp
  Future<int> getAssertTimestamp() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'AssertTimestamp');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.AssertTimestampMonotonic
  Future<int> getAssertTimestampMonotonic() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'AssertTimestampMonotonic');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Conditions
  Future<List<DBusValue>> getConditions() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'Conditions');
    return (value as DBusArray).children.map((child) => child).toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.Asserts
  Future<List<DBusValue>> getAsserts() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Asserts');
    return (value as DBusArray).children.map((child) => child).toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.LoadError
  Future<DBusValue> getLoadError() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'LoadError');
    return value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Transient
  Future<bool> getTransient() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Transient');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Perpetual
  Future<bool> getPerpetual() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Perpetual');
    return (value as DBusBoolean).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.StartLimitIntervalUSec
  Future<int> getStartLimitIntervalUSec() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'StartLimitIntervalUSec');
    return (value as DBusUint64).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.StartLimitBurst
  Future<int> getStartLimitBurst() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'StartLimitBurst');
    return (value as DBusUint32).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.StartLimitAction
  Future<String> getStartLimitAction() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'StartLimitAction');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.FailureAction
  Future<String> getFailureAction() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'FailureAction');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.FailureActionExitStatus
  Future<int> getFailureActionExitStatus() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'FailureActionExitStatus');
    return (value as DBusInt32).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.SuccessAction
  Future<String> getSuccessAction() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'SuccessAction');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.SuccessActionExitStatus
  Future<int> getSuccessActionExitStatus() async {
    var value = await getProperty(
        'org.freedesktop.systemd1.Unit', 'SuccessActionExitStatus');
    return (value as DBusInt32).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.RebootArgument
  Future<String> getRebootArgument() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'RebootArgument');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.InvocationID
  Future<List<int>> getInvocationID() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'InvocationID');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusByte).value)
        .toList();
  }

  /// Gets org.freedesktop.systemd1.Unit.CollectMode
  Future<String> getCollectMode() async {
    var value =
        await getProperty('org.freedesktop.systemd1.Unit', 'CollectMode');
    return (value as DBusString).value;
  }

  /// Gets org.freedesktop.systemd1.Unit.Refs
  Future<List<String>> getRefs() async {
    var value = await getProperty('org.freedesktop.systemd1.Unit', 'Refs');
    return (value as DBusArray)
        .children
        .map((child) => (child as DBusString).value)
        .toList();
  }

  /// Invokes org.freedesktop.systemd1.Unit.Start()
  Future<String> callStart(String arg_0) async {
    var result = await callMethod(
        'org.freedesktop.systemd1.Unit', 'Start', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.Stop()
  Future<String> callStop(String arg_0) async {
    var result = await callMethod(
        'org.freedesktop.systemd1.Unit', 'Stop', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.Reload()
  Future<String> callReload(String arg_0) async {
    var result = await callMethod(
        'org.freedesktop.systemd1.Unit', 'Reload', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.Restart()
  Future<String> callRestart(String arg_0) async {
    var result = await callMethod(
        'org.freedesktop.systemd1.Unit', 'Restart', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.TryRestart()
  Future<String> callTryRestart(String arg_0) async {
    var result = await callMethod(
        'org.freedesktop.systemd1.Unit', 'TryRestart', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.ReloadOrRestart()
  Future<String> callReloadOrRestart(String arg_0) async {
    var result = await callMethod('org.freedesktop.systemd1.Unit',
        'ReloadOrRestart', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.ReloadOrTryRestart()
  Future<String> callReloadOrTryRestart(String arg_0) async {
    var result = await callMethod('org.freedesktop.systemd1.Unit',
        'ReloadOrTryRestart', [DBusString(arg_0)]);
    return (result.returnValues[0] as DBusObjectPath).value;
  }

  /// Invokes org.freedesktop.systemd1.Unit.EnqueueJob()
  Future<List<DBusValue>> callEnqueueJob(String arg_0, String arg_1) async {
    var result = await callMethod('org.freedesktop.systemd1.Unit', 'EnqueueJob',
        [DBusString(arg_0), DBusString(arg_1)]);
    return result.returnValues;
  }

  /// Invokes org.freedesktop.systemd1.Unit.Kill()
  Future callKill(String arg_0, int arg_1) async {
    await callMethod('org.freedesktop.systemd1.Unit', 'Kill',
        [DBusString(arg_0), DBusInt32(arg_1)]);
  }

  /// Invokes org.freedesktop.systemd1.Unit.ResetFailed()
  Future callResetFailed() async {
    await callMethod('org.freedesktop.systemd1.Unit', 'ResetFailed', []);
  }

  /// Invokes org.freedesktop.systemd1.Unit.SetProperties()
  Future callSetProperties(bool arg_0, List<DBusValue> arg_1) async {
    await callMethod('org.freedesktop.systemd1.Unit', 'SetProperties', [
      DBusBoolean(arg_0),
      DBusArray(DBusSignature('(sv)'), arg_1.map((child) => child))
    ]);
  }

  /// Invokes org.freedesktop.systemd1.Unit.Ref()
  Future callRef() async {
    await callMethod('org.freedesktop.systemd1.Unit', 'Ref', []);
  }

  /// Invokes org.freedesktop.systemd1.Unit.Unref()
  Future callUnref() async {
    await callMethod('org.freedesktop.systemd1.Unit', 'Unref', []);
  }

  /// Invokes org.freedesktop.systemd1.Unit.Clean()
  Future callClean(List<String> arg_0) async {
    await callMethod('org.freedesktop.systemd1.Unit', 'Clean', [
      DBusArray(DBusSignature('s'), arg_0.map((child) => DBusString(child)))
    ]);
  }
}
