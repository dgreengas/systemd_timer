// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked_services/stacked_services.dart';

import '../util/services/pick_script_service.dart';
import '../util/services/third_party_services_module.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final thirdPartyServicesModule = _$ThirdPartyServicesModule();

  // Eager singletons must be registered in the right order
  gh.singleton<DialogService>(thirdPartyServicesModule.dialogService);
  gh.singleton<NavigationService>(thirdPartyServicesModule.navigationService);
  gh.singleton<PickScriptService>(thirdPartyServicesModule.pickScriptService);
  gh.singleton<SnackbarService>(thirdPartyServicesModule.snackbarService);
  return get;
}

class _$ThirdPartyServicesModule extends ThirdPartyServicesModule {
  @override
  DialogService get dialogService => DialogService();
  @override
  NavigationService get navigationService => NavigationService();
  @override
  PickScriptService get pickScriptService => PickScriptService();
  @override
  SnackbarService get snackbarService => SnackbarService();
}
