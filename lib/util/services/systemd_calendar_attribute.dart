import 'package:systemd_timer/util/services/systemd_attribute.dart';

class SystemdCalendarAttribute extends SystemdAttribute {
  String dayOfWeek;
  String year;
  String month;
  String day;
  String hour;
  String minutes;
  String seconds;

  SystemdCalendarAttribute(
      {this.dayOfWeek,
      this.year,
      this.month,
      this.day,
      this.hour,
      this.minutes,
      this.seconds})
      : super(
            name: "OnCalendar",
            value: (dayOfWeek ?? "") +
                " " +
                (year ?? "*") +
                "-" +
                (month ?? "*") +
                "-" +
                (day ?? "*") +
                " " +
                (hour ?? "*") +
                ":" +
                (minutes ?? "*") +
                ":" +
                (seconds ?? "*"));
}
