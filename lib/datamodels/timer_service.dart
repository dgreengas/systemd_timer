class TimerService {
  String description;
  String command;

  // format DayOfWeek, Year, Month, Day Hour:Minute:Second
  String when;

  //maps to persistent
  bool runIfMissed;

  TimerService({this.description, this.command, this.when, this.runIfMissed});
}
