import 'package:file_chooser/file_chooser.dart';

class PickScriptService {
  Future<String> chooseScript() async {
    final result = await showOpenPanel(
      allowedFileTypes: [
        FileTypeFilterGroup(fileExtensions: ["sh", ""], label: "Scripts")
      ],
      allowsMultipleSelection: false,
      canSelectDirectories: false,
    );
    if (result.canceled || result.paths.isEmpty) {
      return null;
    }

    return result.paths[0];
  }
}
