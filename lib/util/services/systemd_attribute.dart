class SystemdAttribute {
  String name;
  String value;

  SystemdAttribute({this.name, this.value});
}
