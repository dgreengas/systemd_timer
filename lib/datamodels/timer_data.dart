//result of d-bus call
import 'package:systemd_timer/datamodels/timers_calendar.dart';
import 'package:systemd_timer/datamodels/timers_monotonic.dart';

class TimerData {
  bool persistent;
  bool wakeSystem;
  bool onClockChange;
  bool onTimezoneChange;
  List<TimersCalendar> timersCalendar;
  List<TimersMonotonic> timersMonotonic;
  bool remainAfterElapse;
  String result;
  String unit;
}
