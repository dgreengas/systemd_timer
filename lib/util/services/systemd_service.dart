import 'package:dbus/dbus.dart';

import 'timer_server.dart';

class SystemdService {
  void getRunningTimers() async {
    DBusClient client = DBusClient.system();

    /*var timerService = OrgFreedesktopDBusPeer(
        client, 'org.freedesktop.systemd1',
        path: DBusObjectPath(
            '/org/freedesktop/systemd1/unit/apt_2ddaily_2etimer'));*/

    var timerService = OrgFreedesktopDBusPeer(
        client, 'org.freedesktop.systemd1',
        path: DBusObjectPath('/org/freedesktop/systemd1/unit'));
    List<DBusIntrospectNode> nodes = await timerService.introspect();

    nodes.forEach((node) {
      introspectNode(node);
    });

    client.close();
  }

  introspectNode(DBusIntrospectNode node) {
    if ((node.name ?? "").contains("timer")) {
      print("Name: ${node.name} Interface Count: ${node.interfaces.length}");

      node.interfaces.forEach((interf) {
        print("\tInterface: ${interf.name}");
      });
    }
    node.children.forEach((childNode) => introspectNode(childNode));
  }
}
