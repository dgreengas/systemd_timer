import 'package:injectable/injectable.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:systemd_timer/util/services/pick_script_service.dart';

@module
abstract class ThirdPartyServicesModule {
  @singleton
  NavigationService get navigationService;

  @singleton
  DialogService get dialogService;

  @singleton
  PickScriptService get pickScriptService;

  @singleton
  SnackbarService get snackbarService;
}
