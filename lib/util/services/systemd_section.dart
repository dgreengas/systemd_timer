import 'package:systemd_timer/util/services/systemd_attribute.dart';

class SystemdSection {
  String name;
  List<SystemdAttribute> _attributes = List<SystemdAttribute>();

  List<SystemdAttribute> get attributes => this._attributes;

  SystemdSection({this.name});

  void addAttribute(SystemdAttribute attribute) {
    _attributes.add(attribute);
  }
}
