import 'dart:convert';
import 'dart:io';

import 'package:systemd_timer/util/services/systemd_attribute.dart';
import 'package:systemd_timer/util/services/systemd_section.dart';

class SystemdUnit {
  List<SystemdSection> _sections = List<SystemdSection>();

  List<SystemdSection> get sections => _sections;

  SystemdSection addSection(String name) {
    SystemdSection newSection = SystemdSection(name: name);
    _sections.add(newSection);
    return newSection;
  }

  SystemdUnit();

  SystemdUnit.fromFile(String fileName) {
    final file = new File('file.txt');
    SystemdSection currentSection;

    Stream<List<int>> inputStream = file.openRead();
    inputStream
        .transform(utf8.decoder) // Decode bytes to UTF-8.
        .transform(new LineSplitter()) // Convert stream to individual lines.
        .listen((String line) {
      // Process results.
      if (line.startsWith("\[")) {
        //section line
        currentSection =
            SystemdSection(name: line.substring(1, line.length - 1));
      } else {
        if (line.trim().length > 0) {
          //value line
          List<String> attribute = line.split("=");
          if (attribute.length == 2) {
            //name value
            SystemdAttribute newAttribute =
                SystemdAttribute(name: attribute[0], value: attribute[1]);

            currentSection.addAttribute(newAttribute);
          }
        }
      }
    }, onDone: () {
      print('File is now closed.');
    }, onError: (e) {
      print(e.toString());
    });
  }

  Future<bool> saveToFile(String path) async {
    File file = new File(path);
    var sink = file.openWrite();

    _sections.forEach((section) {
      sink.writeln("[" + section.name + "]");
      section.attributes.forEach((attribute) {
        sink.writeln(attribute.name + "=" + attribute.value.trim());
      });
    });

    sink.close();
    return true;
  }
}
