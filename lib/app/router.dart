import 'package:auto_route/auto_route_annotations.dart';

import 'package:systemd_timer/ui/views/home/home_view.dart';
import 'package:systemd_timer/ui/views/new_task/new_task_view.dart';

@MaterialAutoRouter(routes: <AutoRoute>[
  MaterialRoute(page: HomeView, initial: true),
  MaterialRoute(page: NewTaskView),
])
class $AppRouter {}
