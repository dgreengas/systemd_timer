import 'dart:io';

import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:systemd_timer/app/locator.dart';
import 'package:systemd_timer/util/services/pick_script_service.dart';
import 'package:systemd_timer/util/services/systemd_attribute.dart';
import 'package:process_run/process_run.dart';
import 'package:systemd_timer/util/services/systemd_unit.dart';

class NewTaskViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final PickScriptService pickScriptService = locator<PickScriptService>();
  final SnackbarService snackbarService = locator<SnackbarService>();
  final _dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  String _nextElapsed;
  String _normalizedForm;

  String get nextElapsed => _nextElapsed ?? "";

  String _description = "Schedule a new task";
  String get description => _description;
  String scriptPath = "";

  bool everyDay = true;
  bool everyHour = false;
  bool everyMinute = false;
  bool everySecond = false;

  List<bool> _everyDay = List<bool>.generate(7, (index) => true);
  List<bool> _daySelected = List<bool>.generate(7, (index) => true);
  List<int> _daySelectedInternal = List<int>();
  List<bool> _hourSelected = List<bool>.generate(24, (index) => false);
  List<int> _hourSelectedInternal = List<int>();
  List<bool> _minuteSelected =
      List<bool>.generate(60, (index) => index == 0 ? true : false);
  List<int> _minuteSelectedInternal = List<int>();
  List<bool> _secondSelected =
      List<bool>.generate(60, (index) => index == 0 ? true : false);
  List<int> _secondSelectedInternal = List<int>();
  //defaults to running every day
  List<bool> get daySelected => everyDay ? _everyDay : _daySelected;

  //default to running every hour
  List<bool> get hourSelected =>
      everyHour ? List<bool>.generate(24, (index) => true) : _hourSelected;

  List<bool> get minuteSelected =>
      everyMinute ? List<bool>.generate(60, (index) => true) : _minuteSelected;

  List<bool> get secondSelected =>
      everySecond ? List<bool>.generate(60, (index) => true) : _secondSelected;

  NewTaskViewModel() {
    _hourSelectedInternal.add(0);
    _minuteSelectedInternal.add(0);
    _secondSelectedInternal.add(0);
  }
  void selectDay(int day, bool selected) {
    _daySelected[day] = selected;
    if (!selected && everyDay) {
      everyDay = !everyDay;
    }
    if (selected) {
      _daySelectedInternal.add(day);
    } else {
      _daySelectedInternal.remove(day);
    }
    _updateCalendarEntries();
    notifyListeners();
  }

  void selectHour(int hour, bool selected) {
    _hourSelected[hour] = selected;
    if (selected) {
      _hourSelectedInternal.add(hour);
    } else {
      _hourSelectedInternal.remove(hour);
    }
    _updateCalendarEntries();
    notifyListeners();
  }

  void selectMinute(int minute, bool selected) {
    _minuteSelected[minute] = selected;
    if (selected) {
      _minuteSelectedInternal.add(minute);
    } else {
      _minuteSelectedInternal.remove(minute);
    }
    _updateCalendarEntries();
    notifyListeners();
  }

  void selectSecond(int second, bool selected) {
    _secondSelected[second] = selected;
    if (selected) {
      _secondSelectedInternal.add(second);
    } else {
      _secondSelectedInternal.remove(second);
    }
    _updateCalendarEntries();
    notifyListeners();
  }

  void runEveryDay(bool newValue) {
    everyDay = newValue;
    notifyListeners();
  }

  void runEveryHour(bool newValue) {
    everyHour = newValue;
    notifyListeners();
  }

  void runEveryMinute(bool newValue) {
    everyMinute = newValue;
    notifyListeners();
  }

  void runEverySecond(bool newValue) {
    everySecond = newValue;
    notifyListeners();
  }

  void chooseScript() async {
    final newPath = await pickScriptService.chooseScript();
    if (newPath != null) {
      scriptPath = newPath;
      notifyListeners();
    }
  }

  void saveTask() async {
    //save the details in the task and if successful, navigate back
    bool successful;
    final SystemdUnit timerUnit = SystemdUnit();

    timerUnit.addSection("Unit").addAttribute(
        SystemdAttribute(name: "Description", value: this.description));

    timerUnit.addSection("Timer").addAttribute(
        SystemdAttribute(name: "OnCalendar", value: _normalizedForm));
    final String fileName =
        "/home/dgreengas/.local/share/systemd/user/test.timer";
    successful = await timerUnit.saveToFile(fileName);

    if (successful) {
      //show toast
      snackbarService.showSnackbar(
          message: "Your service was saved to $fileName");
      //navigate back
      _navigationService.back();
    }
  }

  String _calcHourString() {
    if (everyHour) return "*";

    return _hourSelectedInternal.join(",");
  }

  String _calcMinuteString() {
    if (everyMinute) return "*";

    return _minuteSelectedInternal.join(",");
  }

  String _calcSecondString() {
    if (everySecond) return "*";

    return _secondSelectedInternal.join(",");
  }

  String _calcDayOfWeekString() {
    if (everyDay) return "";

    List<String> stringDays = _daySelectedInternal.map((e) => _dayNames[e]);

    return stringDays.join(",");
  }

  void setDescription(String value) {
    this._description = value;
    notifyListeners();
  }

  void _updateCalendarEntries() async {
    String calendarString = ((_calcDayOfWeekString() ?? "") +
            " " +
            (_calcHourString() ?? "*") +
            ":" +
            (_calcMinuteString() ?? "*") +
            ":" +
            (_calcSecondString() ?? "*"))
        .trim();

    ProcessResult result =
        await run('systemd-analyze', ['calendar', '$calendarString']);

    if (result.exitCode == 0) {
      //success now parse stdout
      List<String> lines = result.stdout.toString().split("\n");
      print(lines.length);
      lines.forEach((line) {
        print(line);
        if (line.trim().startsWith("Normalized form")) {
          _normalizedForm = line.substring(line.indexOf(":") + 1).trim();
        } else if (line.trim().startsWith("Next elapse")) {
          _nextElapsed = line.substring(line.indexOf(":") + 1).trim();
          notifyListeners();
          print("nextElapsed: $_nextElapsed");
        }
      });
    }
    // print(
    // "stdout: ${result.stdout.toString()} stderr: ${result.stderr.toString()}");
  }
}
