import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'home_viewmodel.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      builder: (BuildContext context, HomeViewModel model, Widget child) =>
          Scaffold(
        appBar: AppBar(
          title: Text(model.title),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => model.navigateToNewTask(),
          child: Icon(Icons.add_to_queue),
        ),
      ),
      viewModelBuilder: () => HomeViewModel(),
    );
  }
}
